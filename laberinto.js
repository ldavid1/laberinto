var matrizN = [
	[0, 0, 0, 1, 1],
	[0, 0, 1, 1, 1],
	[0, 0, 1, 0, 1],
	[0, 0, 1, 0, 1],
	[0, 0, 3, 0, 0]
];

$(document).ready(function () {

    var tabla  = $("#miTabla");

    matrizN.forEach(
		(row) => {
			row.forEach(
				(col) => {
					console.log(col)
				}
			)
		}
	)

	for (var i = 0; i < matrizN.length; i++) {
        var fila = '<tr>';
		for (var j = 0; j < matrizN[i].length; j++) {
            console.log("for N" + matrizN[i][j]);
            var celda = '<td>'+ matrizN[i][j]+ '</td>';
            fila += celda;            
        }
        fila += '</tr>';
        tabla.append(fila);
    }    

	solve();

});

function solve() {

	console.log(new Date().getMilliseconds());

	var actuallyDirection = 'b'; //l: left, r:right, t:top, b:bottom 
	var isSolved = false;
	var actuallyPosition = [0, 4];	
    var path = [];
    var solucion = '';

	while(!isSolved){
		path.push(actuallyPosition[0] + ',' + actuallyPosition[1]);
		if(matrizN[actuallyPosition[0]][actuallyPosition[1]] === 3){
            isSolved = true;
            for(i=0; i<path.length; i++){
               solucion  += '"'+ path[i] + '" - ';               
            }
            $("#sol").val(solucion);
			console.log(path);
			console.log(new Date().getMilliseconds());
		} else {
			switch(actuallyDirection){
				case 'l': {
					if(matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] - 1, actuallyPosition[1]];
						actuallyDirection = 't';
					} else if(matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] + 1];
					} else if(matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] + 1, actuallyPosition[1]];
						actuallyDirection = 'b';
					} else if(matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] - 1];
						actuallyDirection = 'r';
					}
					break;
				}
				case 'r': {
					if(matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] + 1, actuallyPosition[1]];
						actuallyDirection = 'b';
					} else if(matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] - 1];
					} else if(matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] - 1, actuallyPosition[1]];
						actuallyDirection = 't';
					} else if(matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] + 1];
						actuallyDirection = 'l';
					}
					break;
				}
				case 't': {
					if(matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] - 1];
						actuallyDirection = 'r';
					} else if(matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] - 1, actuallyPosition[1]];
					} else if(matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] + 1];
						actuallyDirection = 'l';
					} else if(matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] + 1, actuallyPosition[1]];
						actuallyDirection = 'b';
					}
					break;
				}
				case 'b': {
					if(matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] + 1];
						actuallyDirection = 'l';
					} else if(matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] + 1, actuallyPosition[1]];
					} else if(matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] - 1];
						actuallyDirection = 'r';
					} else if(matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] - 1, actuallyPosition[1]];
						actuallyDirection = 't';
					}
					break;
				}
			}
		}
	}

}